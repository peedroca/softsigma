﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma
{
    public partial class frmIniciar : Form
    {
        public frmIniciar()
        {
            InitializeComponent();
        }

        private void CarregarScreen(UserControl screen)
        {
            if (pnPrincipal.Controls.Count == 1)
                pnPrincipal.Controls.RemoveAt(0);
            pnPrincipal.Controls.Add(screen);
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modulos.ControleDeCliente.frmPrincipal screen = new Modulos.ControleDeCliente.frmPrincipal();
            CarregarScreen(screen);
        }

        private void funcionarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modulos.ControleDeFuncionario.frmPrincipal screen = new Modulos.ControleDeFuncionario.frmPrincipal();
            CarregarScreen(screen);
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void peçasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modulos.ControleDePecas.consultar screen = new Modulos.ControleDePecas.consultar();
            screen.Show();
        }

        private void orçamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modulos.ControleDeOrcamento.frmConsultar screen = new Modulos.ControleDeOrcamento.frmConsultar();
            CarregarScreen(screen);
        }

        private void atendimentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modulos.ControleDeAtendimento.cadastrar frm = new Modulos.ControleDeAtendimento.cadastrar();
            frm.Show();
        }
    }
}

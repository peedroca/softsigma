﻿using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Atendimento
{
    class AtendimentoBusiness
    {
        public int Cadastrar(AtendimentoDTO dto)
        {
            if (dto.Situacao == string.Empty)
                throw new ArgumentException("Situação não pode estar em branco!");

            if (dto.descricao == string.Empty)
                throw new ArgumentException("Descrição não pode estar em branco!");

            AtendimentoDatabase db = new AtendimentoDatabase();
            return db.Cadastrar(dto);
        }

        public int Alterar(AtendimentoDTO dto)
        {
            if (dto.Situacao == string.Empty)
                throw new ArgumentException("Situação não pode estar em branco!");

            if (dto.descricao == string.Empty)
                throw new ArgumentException("Descrição não pode estar em branco!");

            AtendimentoDatabase db = new AtendimentoDatabase();
            return db.Alterar(dto);
        }

        public List<AtendimentoDTO> ListarPecas(AtendimentoDTO dto)
        {
            AtendimentoDatabase db = new AtendimentoDatabase();
            return db.ListarPecas(dto);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Atendimento
{
    class AtendimentoDTO
    {
        public int Id_Atendimento { get; set; }
        public DateTime data { get; set; }
        public string descricao { get; set; }
        public int Funcionario_id_Funcionario { get; set; }
        public int Orcamento_id_Orcamento { get; set; }
        public int Cliente_id_Cliente { get; set; }
        public DateTime Horario { get; set; }
        public string Situacao { get; set; }
    }
}

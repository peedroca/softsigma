﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Atendimento
{
    class AtendimentoDatabase
    {
        public int Cadastrar(AtendimentoDTO dto)
        {
            string script = @"INSERT INTO pecas(Data, Descricao, Horario, Situacao, Funcionario_id_funcionario, Orcamento_id_orcamento, cliente_id_cliente) 
                                         VALUES(@Data, @Descricao, @Horario, @Situacao, @Funcionario_id_funcionario, @Orcamento_id_orcamento, @cliente_id_cliente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Data", dto.data));
            parms.Add(new MySqlParameter("Descricao", dto.descricao));
            parms.Add(new MySqlParameter("Horario", DateTime.Now));
            parms.Add(new MySqlParameter("Situacao", dto.Situacao));
            parms.Add(new MySqlParameter("Funcionario_id_funcionario", dto.Funcionario_id_Funcionario));
            parms.Add(new MySqlParameter("Orcamento_id_orcamento", dto.Orcamento_id_Orcamento));
            parms.Add(new MySqlParameter("cliente_id_cliente", dto.Cliente_id_Cliente));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public int Alterar(AtendimentoDTO dto)
        {
            string script = @"UPDATE pecas 
                                 SET Data = @Data,
                                     Descricao = @Descricao,
                                     Situacao = @Situacao,
                                     Funcionario_id_funcionario = @Funcionario_id_funcionario,
                                     cliente_id_cliente = @cliente_id_cliente
                               WHERE idAtendimento = @idAtendimento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Data", dto.data));
            parms.Add(new MySqlParameter("Descricao", dto.descricao));
            parms.Add(new MySqlParameter("Situacao", dto.Situacao));
            parms.Add(new MySqlParameter("Funcionario_id_funcionario", dto.Funcionario_id_Funcionario));
            parms.Add(new MySqlParameter("cliente_id_cliente", dto.Cliente_id_Cliente));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        List<MySqlParameter> parms = new List<MySqlParameter>();
        string script = string.Empty;

        public List<AtendimentoDTO> ListarPecas(AtendimentoDTO dto)
        {
            if(dto.Id_Atendimento == 0)
            {
                script = @"SELECT * FROM pecas";
            }
            else
            {
                script = @"SELECT * FROM pecas WHERE idAtendimento = @idAtendimento";
                parms.Add(new MySqlParameter("idAtendimento", dto.Id_Atendimento));
            }

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AtendimentoDTO> DTO = new List<AtendimentoDTO>();
            while (reader.Read())
            {
                AtendimentoDTO Dto = new AtendimentoDTO();
                Dto.Id_Atendimento = reader.GetInt32("idAtendimento");
                Dto.data = reader.GetDateTime("Data");
                Dto.descricao = reader.GetString("Descricao");
                Dto.Horario = reader.GetDateTime("Horario");
                Dto.Situacao = reader.GetString("Situacao");
                Dto.Funcionario_id_Funcionario = reader.GetInt32("Funcionario_id_funcionario");
                Dto.Orcamento_id_Orcamento = reader.GetInt32("Orcamento_id_orcamento");
                Dto.Cliente_id_Cliente = reader.GetInt32("cliente_id_cliente");

                DTO.Add(Dto);
            }
            reader.Close();
            return DTO;
        }
    }
}

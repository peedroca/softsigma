﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Funcionario
{
    class FuncionarioDTO
    {
        public int id_funcionario { get; set; }
        public string usuario { get; set; }
        public string Nome { get; set; }
        public DateTime Data_Nascimento { get; set; }
        public string cpf { get; set; }
        public string Rg { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Observacao { get; set; }
        public string Cargo { get; set; }

    }
}

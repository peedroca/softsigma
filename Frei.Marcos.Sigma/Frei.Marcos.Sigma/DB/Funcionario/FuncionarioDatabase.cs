﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Funcionario
{
    class FuncionarioDatabase
    {
        public int Cadastrar(FuncionarioDTO dto)
        {
            string script = @"INSERT INTO Funcionario(Usuario, Nome, Data_nascimento, CPF, RG, Endereco, Complemento, Cargo, observacao) 
                                         VALUES(@Usuario, @Nome, @Data_nascimento, @CPF, @RG, @Endereco, @Complemento, @Cargo, @observacao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Usuario", dto.usuario));
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Data_nascimento", dto.Data_Nascimento));
            parms.Add(new MySqlParameter("CPF", dto.cpf));
            parms.Add(new MySqlParameter("RG", dto.Rg));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("Complemento", dto.Complemento));
            parms.Add(new MySqlParameter("Cargo", dto.Cargo));
            parms.Add(new MySqlParameter("observacao", dto.Observacao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public int Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE Funcionario SET Usuario = @Usuario,
                                               Endereco = @Endereco,
                                               Complemento = @Complemento,
                                               Cargo = @Cargo,
                                               observacao = @observacao
                                         WHERE idFuncionario = @idFuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Usuario", dto.usuario));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("Complemento", dto.Complemento));
            parms.Add(new MySqlParameter("Cargo", dto.Cargo));
            parms.Add(new MySqlParameter("observacao", dto.Observacao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FuncionarioDTO> ListarPecas()
        {
            string script = @"SELECT * FROM pecas";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> DTO = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.id_funcionario = reader.GetInt32("idFuncionario");
                dto.usuario = reader.GetString("Usuario");
                dto.Nome = reader.GetString("Nome");
                dto.Data_Nascimento = reader.GetDateTime("Data_nascimento");
                dto.cpf = reader.GetString("CPF");
                dto.Rg = reader.GetString("RG");
                dto.Endereco = reader.GetString("Endereco");
                dto.Complemento = reader.GetString("Complemento");
                dto.Cargo = reader.GetString("Cargo");
                dto.Observacao = reader.GetString("observacao");

                DTO.Add(dto);
            }
            reader.Close();
            return DTO;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Funcionario
{
    class FuncionarioBusiness
    {
        public int Cadastrar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Cadastrar(dto);
        }

        public int Alterar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Alterar(dto);
        }

        public List<FuncionarioDTO> ListarPecas()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.ListarPecas();
        }
    }
}

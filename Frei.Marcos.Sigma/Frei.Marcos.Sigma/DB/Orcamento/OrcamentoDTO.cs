﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Orcamento
{
    class OrcamentoDTO
    {
        public int Id_Orcamento { get; set; }
        public DateTime Data { get; set; }
        public string Descricao { get; set; }
        public double Valor { get; set; }
        public string Situacao { get; set; }
        public int Funcionario_id_Funcionario { get; set; }
    }
}

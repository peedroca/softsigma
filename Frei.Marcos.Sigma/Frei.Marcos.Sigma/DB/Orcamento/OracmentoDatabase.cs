﻿using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Orcamento
{
    class OracmentoDatabase
    {
        public int Cadastrar(OrcamentoDTO dto)
        {
            string script = @"INSERT INTO orcamento(data, descricao, valor, situacao, funcionario_id_funcionario)
                                           VALUES(@data, @descricao, @valor, @situacao, @funcionario_id_funcionario");

        
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("data", dto.Data));
            parms.Add(new MySqlParameter("descricao", dto.Descricao));
            parms.Add(new MySqlParameter("valor", dto.Valor));
            parms.Add(new MySqlParameter("situacao", dto.Situacao));
            parms.Add(new MySqlParameter("funcionario_id_funcionario", dto.Funcionario_id_Funcionario));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public int Alterar(OrcamentoDTO dto)
        {
            string script = @"UPDATE pecas SET data = @data, descricao = @descricao, valor = @valor, situacao = @situacao, funcionario_id_funcionario = @funcionario_id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("data", dto.Data));
            parms.Add(new MySqlParameter("descricao", dto.Descricao));
            parms.Add(new MySqlParameter("valor", dto.Valor));
            parms.Add(new MySqlParameter("situacao", dto.Situacao));
            parms.Add(new MySqlParameter("funcionario_id_funcionario", dto.Funcionario_id_Funcionario));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<OrcamentoDTO> ListarOrcamento()
        {
            string script = @"SELECT * FROM orcamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<OrcamentoDTO> DTO = new List<OrcamentoDTO>();
            while (reader.Read())
            {
                OrcamentoDTO dto = new OrcamentoDTO();
                dto.Id_Orcamento = reader.GetInt32("id_orcamento");
                dto.Data = reader.GetDate("data");
                dto.Descricao = reader.GetString("descricao");
                dto.Valor = reader.GetInt32("valor");
                dto.Situacao = reader.GetString("situacao");
                dto.Funcionario_id_Funcionario = reader.GetInt32("funcionario_id_funcionario");

                DTO.Add(dto);
            }
            reader.Close();
            return DTO;
        }
             



    }
}

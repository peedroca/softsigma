﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Cliente
{
    class ClienteDTO
    {
        public int Id_Cliente { get; set; }
        public string Cpf_Cnpj { get; set; }
        public string Razao_social { get; set; }
        public string Nome { get; set; }
        public DateTime Data_Nascimento { get; set; }
        public string cep { get; set; }
        public string Estado { get; set; }
        public string cidade { get; set; }
        public string bairro { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
       
    }
}

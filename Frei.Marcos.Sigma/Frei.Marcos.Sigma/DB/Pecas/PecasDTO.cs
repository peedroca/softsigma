﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Pecas
{
    class PecasDTO
    {
        public int Id_pecas { get; set; }
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public double Valor { get; set; }
        public string descricao { get; set; }
        public int Orcamento_id_Orcamento { get; set; }
    }
}

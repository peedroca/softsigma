﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Pecas
{
    class PecasBusiness
    {
        public int Cadastrar(PecasDTO dto)
        {
            if (dto.Nome == string.Empty)
                throw new ArgumentException("O nome não pode ser vázio!");

            if (dto.Quantidade == 0)
                throw new ArgumentException("A quantidade não pode ser nula!");

            if (dto.Valor == 0)
                throw new ArgumentException("O valor não pode ser nulo!");

            PecasDatabase db = new PecasDatabase();
            return db.Cadastrar(dto);
        }

        public int Alterar(PecasDTO dto)
        {
            if (dto.Quantidade == 0)
                throw new ArgumentException("A quantidade não pode ser nula!");

            if (dto.Valor == 0)
                throw new ArgumentException("O valor não pode ser nulo!");

            PecasDatabase db = new PecasDatabase();
            return db.Alterar(dto);
        }

        public List<PecasDTO> ListarPecas()
        {
            PecasDatabase db = new PecasDatabase();
            return db.ListarPecas();
        }
    }
}

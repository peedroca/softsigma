﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frei.Marcos.Sigma.DB.Pecas
{
    class PecasDatabase
    {
        public int Cadastrar(PecasDTO dto)
        {
            string script = @"INSERT INTO pecas(nome, quantidade, valor, descricao, orcamento_id_orcamento) 
                                         VALUES(@nome, @quantidade, @valor, @descricao, @orcamento_id_orcamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.Nome));
            parms.Add(new MySqlParameter("quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("valor", dto.Valor));
            parms.Add(new MySqlParameter("descricao", dto.descricao));
            parms.Add(new MySqlParameter("orcamento_id_orcamento", dto.Orcamento_id_Orcamento));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public int Alterar(PecasDTO dto)
        {
            string script = @"UPDATE pecas SET quantidade = @quantidade, valor = @valor, descricao = @descricao WHERE id_pecas = @id_pecas";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("valor", dto.Valor));
            parms.Add(new MySqlParameter("descricao", dto.descricao));
            parms.Add(new MySqlParameter("id_pecas", dto.Id_pecas));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PecasDTO> ListarPecas()
        {
            string script = @"SELECT * FROM pecas";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PecasDTO> DTO = new List<PecasDTO>();
            while (reader.Read())
            {
                PecasDTO dto = new PecasDTO();
                dto.Id_pecas = reader.GetInt32("id_pecas");
                dto.Nome = reader.GetString("nome");
                dto.Quantidade = reader.GetInt32("quantidade");
                dto.Valor = reader.GetDouble("valor");
                dto.descricao = reader.GetString("descricao");
                dto.Orcamento_id_Orcamento = reader.GetInt32("orcamento_id_orcamento");

                DTO.Add(dto);
            }
            reader.Close();
            return DTO;
        }
    }
}

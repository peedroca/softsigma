﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma.Modulos.ControleDePecas
{
    public partial class consultar : Form
    {
        public consultar()
        {
            InitializeComponent();
        }

        private void consultar_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            cadastrar screen = new cadastrar();
            screen.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            DB.Pecas.PecasBusiness business = new DB.Pecas.PecasBusiness();

            dgvPecas.AutoGenerateColumns = false;
            dgvPecas.DataSource = business.ListarPecas();
        }
    }
}

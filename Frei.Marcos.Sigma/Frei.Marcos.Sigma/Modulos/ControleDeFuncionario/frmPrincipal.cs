﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma.Modulos.ControleDeFuncionario
{
    public partial class frmPrincipal : UserControl
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmAcessos frm = new frmAcessos();
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Você deseja mesmo excluir essa conta?", "SIGMA", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAddFunc frm = new frmAddFunc();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAlterar frm = new frmAlterar();
            frm.Show();
        }
    }
}

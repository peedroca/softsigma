﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma.Modulos.ControleDeAtendimento
{
    public partial class alterar : Form
    {
        public alterar()
        {
            InitializeComponent();
        }

        private void alterar_Load(object sender, EventArgs e)
        {

        }

        private void txtidalterar_TextChanged(object sender, EventArgs e)
        {

        }

        private void btncadastraralterar_Click(object sender, EventArgs e)
        {
            try
            {
                DB.Atendimento.AtendimentoDTO dto = new DB.Atendimento.AtendimentoDTO();
                dto.Situacao = txtSituação.Text;
                dto.Horario = DateTime.Now;
                dto.Orcamento_id_Orcamento = Convert.ToInt32(txtOrc.Text);
                dto.Funcionario_id_Funcionario = Convert.ToInt32(cboFuncionarios.ValueMember);
                dto.Cliente_id_Cliente = Convert.ToInt32(cboCliente.ValueMember);
                dto.data = DateTime.Now;
                dto.descricao = txtdescricao.Text;

                DB.Atendimento.AtendimentoBusiness business = new DB.Atendimento.AtendimentoBusiness();
                business.Alterar(dto);

                MessageBox.Show("Peça cadastrada!", "SIGMA", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "SIGMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma.Modulos.ControleDeAtendimento
{
    public partial class consultar : Form
    {
        public consultar()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnalterarinformaçoes_Click(object sender, EventArgs e)
        {
            alterar frm = new alterar();
            Hide();
            frm.ShowDialog();
            Show();
        }

        private void btnprocurarconsult_Click(object sender, EventArgs e)
        {
            DB.Atendimento.AtendimentoDTO dto = new DB.Atendimento.AtendimentoDTO();
            dto.Id_Atendimento = Convert.ToInt32(txtId.Text);

            DB.Atendimento.AtendimentoBusiness business = new DB.Atendimento.AtendimentoBusiness();
            dgvAtendimento.AutoGenerateColumns = false;
            dgvAtendimento.DataSource = business.ListarPecas(dto);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma.Modulos.ControleDePecas
{
    public partial class cadastrar : Form
    {
        public cadastrar()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                DB.Pecas.PecasDTO dto = new DB.Pecas.PecasDTO();
                dto.Nome = txtPecas.Text;
                dto.Quantidade = Convert.ToInt32(nudQnt.Value);
                dto.Valor = Convert.ToDouble(txtValor.Text);
                dto.descricao = txtDesc.Text;
                dto.Orcamento_id_Orcamento = Convert.ToInt32(lblCod.Text);

                DB.Pecas.PecasBusiness business = new DB.Pecas.PecasBusiness();
                business.Cadastrar(dto);

                MessageBox.Show("Peça cadastrada!", "SIGMA", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "SIGMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

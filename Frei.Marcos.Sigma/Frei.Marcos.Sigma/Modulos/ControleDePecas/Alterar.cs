﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma.Modulos.ControleDePecas
{
    public partial class Alterar : Form
    {
        public Alterar()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DB.Pecas.PecasDTO dto = new DB.Pecas.PecasDTO();
                dto.Quantidade = Convert.ToInt32(nudQnt.Value);
                dto.Valor = Convert.ToDouble(txtValor.Text);
                dto.descricao = txtDesc.Text;

                DB.Pecas.PecasBusiness business = new DB.Pecas.PecasBusiness();
                business.Alterar(dto);

                MessageBox.Show("Peça Alterada!", "SIGMA", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "SIGMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma.Modulos.ControleDeCliente
{
    public partial class frmSelecionar : Form
    {
        public frmSelecionar()
        {
            InitializeComponent();
        }

        private void btnFisica_Click(object sender, EventArgs e)
        {
            frmAddPF frm = new frmAddPF();
            Hide();
            frm.ShowDialog();
            Close();
        }

        private void btnJuridica_Click(object sender, EventArgs e)
        {
            frmAddPJ frm = new frmAddPJ();
            Hide();
            frm.ShowDialog();
            Close();
        }
    }
}

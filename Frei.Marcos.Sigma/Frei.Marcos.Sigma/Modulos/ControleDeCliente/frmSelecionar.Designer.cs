﻿namespace Frei.Marcos.Sigma.Modulos.ControleDeCliente
{
    partial class frmSelecionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnJuridica = new System.Windows.Forms.Button();
            this.btnFisica = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnJuridica
            // 
            this.btnJuridica.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold);
            this.btnJuridica.Location = new System.Drawing.Point(223, 10);
            this.btnJuridica.Name = "btnJuridica";
            this.btnJuridica.Size = new System.Drawing.Size(205, 72);
            this.btnJuridica.TabIndex = 1;
            this.btnJuridica.Text = "Adicionar Pessoa Jurídica";
            this.btnJuridica.UseVisualStyleBackColor = true;
            this.btnJuridica.Click += new System.EventHandler(this.btnJuridica_Click);
            // 
            // btnFisica
            // 
            this.btnFisica.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold);
            this.btnFisica.Location = new System.Drawing.Point(12, 10);
            this.btnFisica.Name = "btnFisica";
            this.btnFisica.Size = new System.Drawing.Size(205, 72);
            this.btnFisica.TabIndex = 2;
            this.btnFisica.Text = "Adicionar Pessoa Física";
            this.btnFisica.UseVisualStyleBackColor = true;
            this.btnFisica.Click += new System.EventHandler(this.btnFisica_Click);
            // 
            // frmSelecionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Frei.Marcos.Sigma.Properties.Resources.download__1_;
            this.ClientSize = new System.Drawing.Size(440, 93);
            this.Controls.Add(this.btnJuridica);
            this.Controls.Add(this.btnFisica);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmSelecionar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SIGMA";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnJuridica;
        private System.Windows.Forms.Button btnFisica;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frei.Marcos.Sigma.Modulos.ControleDeCliente
{
    public partial class frmPrincipal : UserControl
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmSelecionar frm = new frmSelecionar();
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Você deseja mesmo excluir essa conta?", "SIGMA", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboTipo.SelectedItem.ToString() == "Pessoa Física")
                {
                    frmAlterPF frm = new frmAlterPF();
                    frm.Show();
                }
                else
                {
                    frmAlterPJ frm = new frmAlterPJ();
                    frm.Show();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Selecione algum cliente.", "SIGMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
